## Introduction
IVS2vec is the implementation of DeepBindVec applied on the field of inverse virtual screening to look for potential targets for a unknown compound. IVS2vec has been published on Methods. https://doi.org/10.1016/j.ymeth.2019.03.012

DeepBindVec is a model integrating both word2vec technique and fully-connected neural network technique. This model is used to perform knowledge-based binding prediction between protein and molecule. In this work, a package called Mol2vec<sup>[1]</sup>, is employed to vectorize compounds and pocket structures of proteins via word2vec technique. A 600-dimensions vector is generated to represent a combination of compound and the pocket structure. Based on known binding dataset between proteins and molecules. Fully-connected neural network algorithm is used to build up prediction model.


## Instruction
### DFCNN(Dense Fully-Connected Neural Network)

__input__: 600-dimension vector which consists of two 300-dimension vectors transformed from certain protein pocket and compound separately. 

__output__: 0~1 which indicates the probability of the binding of specific protein and ligand.

+ FCdense.h5 is the trained densely fully-connected neural network (DFCNN). It can be loaded using keras.
+ data used to train our model is in "data" folder.
+ DFCNN_predict.ipynb is a demo concerning how to use our model.
<img src="./DFCNN.png"  width="600" height="300">

### word2vec method
The example scripts to generate the vector is in the data_prepare_script folder.

1. The bach_mol2vec.bash script was used to obtain 300 dimension vector of small molecule(ligan) by using mol2vec<sup>[1]</sup>. The detailed usage of mol2vec can be found in their github https://github.com/samoturk/mol2vec.
2. the test_poc.py was used to obtain 300 dimension vector of the pocket aminos.
3. the data_process_all.py  was used to combine the pocket vector with the ligand vector. The run_all.bash is the example of usage.

[1]	Jaeger, S., S. Fulle, and S. Turk, Mol2vec: Unsupervised Machine Learning Approach with Chemical Intuition. J Chem Inf Model, 2018. 58(1): p. 27-35.

