1.the run_x.bash was used to preprocess the PDB files.
2. the run_all.bash was used to execute the fpocket[1].
3.  the run_poc.bash was used to  get the pocket coordinates by the extract.py script.

[1] Le Guilloux, V., P. Schmidtke, and P. Tuffery, Fpocket: an open source platform for ligand pocket detection. BMC Bioinformatics, 2009. 10: p. 168.