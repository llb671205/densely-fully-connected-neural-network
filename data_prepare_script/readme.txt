1. The bach_mol2vec.bash script was used to obtain 300 dimension vector of small molecule(ligan) by using mol2vec[1]. The detailed usage of mol2vec can be found  in their github https://github.com/samoturk/mol2vec.
2. the test_poc.py was used to obtain 300 dimension vector of the pocket aminos.
3. the data_process_all.py  was used to combine the pocket vector with the ligand vector. The run_all.bash is the example of usage.


[1].	Jaeger, S., S. Fulle, and S. Turk, Mol2vec: Unsupervised Machine Learning Approach with Chemical Intuition. J Chem Inf Model, 2018. 58(1): p. 27-35.

